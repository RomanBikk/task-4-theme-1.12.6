import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Solution {
  public static void main(String [] args){
      BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
      try {
          int num = Integer.parseInt(reader.readLine());
          if(num>0){//если введенное число положительное
          for (int i = -(num) ; i<=num; i++){
              if(i!=0){
              if(num%i==0){
                  System.out.println(i);
              }

          }}}
          else if(num<0){// если введенное число отрицательное
              for (int i = -(num) ; i>=num; i--){
                  if(i!=0){
                      if(num%i==0){
                          System.out.println(i);
                      }

                  }}

          }
          else {// если ноль
              System.out.println(num);
          }
      } catch (IOException e) {
          e.printStackTrace();
      }
  }

}
